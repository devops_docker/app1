#! /bin/bash

while true
do
	case $1 in
		--project)
			PROJECT=$2
			shift
			shift
		;;
		--environment)
			ENVIRONMENT=$2
			shift
			shift
		;;
		--help|-h)
			display_usage
            exit 0
        ;;
        -*)
            echo "Unknown option: $1"
            exit 1
        ;;
        *)
            break
        ;;
    esac
done

. /ucp/env.sh;
docker-compose -p ${PROJECT}${ENVIRONMENT} -f docker-compose-${ENVIRONMENT}.yml pull
docker-compose -p ${PROJECT}${ENVIRONMENT} -f docker-compose-${ENVIRONMENT}.yml stop
docker-compose -p ${PROJECT}${ENVIRONMENT} -f docker-compose-${ENVIRONMENT}.yml rm -f
docker-compose -p ${PROJECT}${ENVIRONMENT} -f docker-compose-${ENVIRONMENT}.yml up -d